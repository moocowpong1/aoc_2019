

#[derive(Debug)]
pub enum IntcodeErr {
    OutOfBoundsRead(i64),
    OutOfBoundsWrite(i64, i64),
    OutOfBoundsExecution(usize),
    InvalidOpCode(i64),
    InsufficientInput(i64),
}
use IntcodeErr::*;
use std::{error, fmt};

impl error::Error for IntcodeErr {}
impl fmt::Display for IntcodeErr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            OutOfBoundsRead(addr) => write!(f, "Out of bounds read at address {}", addr),
            OutOfBoundsWrite(addr, val) => write!(f, "Out of bounds write of value {} at address {}", val, addr),
            OutOfBoundsExecution(addr) => write!(f, "Tried to execute at out of bounds address {}", addr),
            InvalidOpCode(opcode) => write!(f, "Tried to execute invalid opcode {}", opcode),
            InsufficientInput(addr) => write!(f, "Insufficient input intended for address {}", addr),
        }
    }
}

pub type IntcodeResult<T> = Result<T, IntcodeErr>;
