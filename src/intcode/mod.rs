
pub mod error;


use std::collections::VecDeque;
use error::{IntcodeErr, IntcodeResult};

pub const ADD: i64  = 1i64;
pub const MUL: i64  = 2i64;
pub const IN: i64   = 3i64;
pub const OUT: i64  = 4i64;
pub const JNZ: i64  = 5i64;
pub const JZ: i64   = 6i64;
pub const LT: i64   = 7i64;
pub const EQ: i64   = 8i64;
pub const HALT: i64 = 99i64;

#[derive(Clone, Copy, Debug)]
struct Parameter {
    val: i64,
    immediate: bool,
}

#[derive(Clone, Copy, Debug)]
enum Opcode {
    Add,
    Multiply,
    Input,
    Output,
    JumpIfTrue,
    JumpIfFalse,
    LessThan,
    Equal,
    Halt,
}
use Opcode::*;

impl Opcode {
    fn instruction_size(&self) -> usize {
        match self {
            Add => 4,
            Multiply => 4,
            Input => 2,
            Output => 2,
            JumpIfTrue => 3,
            JumpIfFalse => 3,
            LessThan => 4,
            Equal => 4,
            Halt => 1,
        }
    }
}

#[derive(Clone, Debug)]
struct Instruction {
    opcode: Opcode,
    params: Vec<Parameter>,
}

#[derive(Clone, Debug)]
pub struct IntcodeMachine {
    memory: Vec<i64>,
    instruction_pointer: usize,
    inputs: VecDeque<i64>,
    outputs: Vec<i64>,
    version: u8,            // version will always be the day of the problem, in case there are breaking changes
}

impl IntcodeMachine {
    // Initialize the intcode machine from a comma-separated string of numbers and a version number.
    pub fn initialize(input: &str, version: u8) -> Result<IntcodeMachine, std::num::ParseIntError>  {
        let mut memory = Vec::new();
        for num in input.split(',') {
            memory.push(num.parse::<i64>()?);
        }
        Ok(IntcodeMachine { memory, version, instruction_pointer: 0, inputs: VecDeque::new(), outputs: Vec::new() })
    }

    pub fn write_input<T: IntoIterator<Item=i64>>(&mut self, iter: T) {
        self.inputs.extend(iter);
    }

    pub fn outputs(&mut self) -> std::vec::Drain<'_, i64> {
        self.outputs.drain(..)
    }

    pub fn memory(&self) -> &Vec<i64> {
        &self.memory
    }

    pub fn memory_mut(&mut self) -> &mut Vec<i64> {
        &mut self.memory
    }

    // The value of a parameter, interpreted either as a read from memory or an immediate load.
    fn read(&self, param: &Parameter) -> IntcodeResult<i64> {
        if param.immediate {
            Ok(param.val)
        } else {
            let address = param.val;
            self.memory.get(address as usize).copied().ok_or_else(|| IntcodeErr::OutOfBoundsRead(address))
        }
    }

    fn write(&mut self, param: &Parameter, value: i64) -> IntcodeResult<()> {
        // The immediate flag is ignored when writing; the value is always interpreted as an address.
        let address = param.val;
        let mem = self.memory.get_mut(address as usize).ok_or_else(|| IntcodeErr::OutOfBoundsWrite(address, value))?;
        *mem = value;
        Ok(())
    }

    // Reads an instruction from the instruction pointer. Does *not* increment the instruction pointer.
    fn read_instruction(&self) -> IntcodeResult<Instruction> {
        let ip = self.instruction_pointer;
        let opcode_val = *(self.memory.get(ip).ok_or_else(|| IntcodeErr::OutOfBoundsExecution(ip))?);
        let (opcode, parameter_modes) = IntcodeMachine::interpret_opcode(opcode_val)?;
        let num_params = opcode.instruction_size() - 1;
        let mut params = Vec::new();
        for i in 0..num_params {
            let val = *(self.memory.get(ip + i + 1).ok_or_else(|| IntcodeErr::OutOfBoundsExecution(ip))?);
            params.push(Parameter { val, immediate: parameter_modes[i] });
        }
        Ok(Instruction { opcode, params })
    }

    fn interpret_opcode(val: i64) -> IntcodeResult<(Opcode, Vec<bool>)> {
        let opcode_part = val.rem_euclid(100);
        let mut flags = (val - opcode_part)/100;
        let opcode = match opcode_part {
            ADD => Ok(Add),
            MUL => Ok(Multiply),
            IN => Ok(Input),
            OUT => Ok(Output),
            JNZ => Ok(JumpIfTrue),
            JZ => Ok(JumpIfFalse),
            LT => Ok(LessThan),
            EQ => Ok(Equal),
            HALT => Ok(Halt),
            _ => Err(IntcodeErr::InvalidOpCode(val)),
        }?;
        let num_params = opcode.instruction_size() - 1;
        let mut parameter_modes = Vec::new();
        for _ in 0..num_params {
            let rem = flags.rem_euclid(10);
            parameter_modes.push(match rem {
                0 => Ok(false),
                1 => Ok(true),
                _ => Err(IntcodeErr::InvalidOpCode(val)),
            }?);
            flags = (flags - rem)/10;
        }
        Ok((opcode, parameter_modes))
    }

    // returns false if the execution halted
    pub fn execution_step(&mut self) -> IntcodeResult<bool> {
        let inst = self.read_instruction()?;
        let mut jumped = false;
        match inst.opcode {
            Add => {
                let val1 = self.read(&inst.params[0])?;
                let val2 = self.read(&inst.params[1])?;
                self.write(&inst.params[2], val1 + val2)?;
            }
            Multiply => {
                let val1 = self.read(&inst.params[0])?;
                let val2 = self.read(&inst.params[1])?;
                self.write(&inst.params[2], val1 * val2)?;
            }
            Input => {
                let addr = inst.params[0];
                let input = self.inputs.pop_front().ok_or_else(|| IntcodeErr::InsufficientInput(addr.val))?;
                self.write(&addr, input)?;
            }
            Output => {
                let val = self.read(&inst.params[0])?;
                self.outputs.push(val);
            }
            JumpIfTrue => {
                let val = self.read(&inst.params[0])?;
                let addr = self.read(&inst.params[1])?;
                if val != 0 {
                    jumped = true;
                    self.instruction_pointer = addr as usize;   // this is dubious if addr is negative, but it should be out of bounds anyway.
                }
            }
            JumpIfFalse => {
                let val = self.read(&inst.params[0])?;
                let addr = self.read(&inst.params[1])?;
                if val == 0 {
                    jumped = true;
                    self.instruction_pointer = addr as usize;
                }
            }
            LessThan => {
                let val1 = self.read(&inst.params[0])?;
                let val2 = self.read(&inst.params[1])?;
                self.write(&inst.params[2], if val1 < val2 {1} else {0})?;
            }
            Equal => {
                let val1 = self.read(&inst.params[0])?;
                let val2 = self.read(&inst.params[1])?;
                self.write(&inst.params[2], if val1 == val2 {1} else {0})?;
            }
            Halt => { return Ok(false); }
        }
        if !jumped { self.instruction_pointer += inst.opcode.instruction_size() };
        Ok(true)
    }

    pub fn execute(&mut self) -> IntcodeResult<()> {
        while self.execution_step()? {}
        Ok(())
    }
}