

pub fn part1(input: &str) -> String {
    let lower_bound = input[0..6].parse::<u32>().unwrap();
    let upper_bound = input[7..13].parse::<u32>().unwrap();

    let mut total_valid = 0;
    let mut cum_val: [u32;6] = [0,0,0,0,0,0];
    for digit0 in 0..=9 {
        cum_val[0] = (digit0 as u32)*100000;
        if cum_val[0] < lower_bound - 100000 || cum_val[0] > upper_bound { continue; }
        for digit1 in digit0..=9 {
            cum_val[1] = cum_val[0] + (digit1 as u32)*10000;
            if cum_val[1] < lower_bound - 10000 || cum_val[1] > upper_bound { continue; }
            for digit2 in digit1..=9 {
                cum_val[2] = cum_val[1] + (digit2 as u32)*1000;
                if cum_val[2] < lower_bound - 1000 || cum_val[2] > upper_bound { continue; }
                for digit3 in digit2..=9 {
                    cum_val[3] = cum_val[2] + (digit3 as u32)*100;
                    if cum_val[3] < lower_bound - 100 || cum_val[3] > upper_bound { continue; }
                    for digit4 in digit3..=9 {
                        cum_val[4] = cum_val[3] + (digit4 as u32)*10;
                        if cum_val[4] < lower_bound - 10 || cum_val[4] > upper_bound { continue; }
                        for digit5 in digit4..=9 {
                            cum_val[5] = cum_val[4] + (digit5 as u32);
                            if cum_val[5] < lower_bound || cum_val[5] > upper_bound { continue; }
                            if (digit0 == digit1) || (digit1 == digit2) || (digit2 == digit3) || (digit3 == digit4) || (digit4 == digit5){
                                total_valid += 1;
                            }
                        }
                    }
                }
            }
        }
    }

    format!("{}", total_valid)
}








#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day04_part1() {
        let input = fs::read_to_string("inputs/day04.txt").unwrap();
        let ans = super::part1(input.as_str());
        assert!(ans == "925");
    }
}