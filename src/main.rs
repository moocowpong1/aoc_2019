
use std::{fs, env};
use aoc_2019::{
    day01,
    day02,
    day03,
    day04,
    day05,
    day06,
    day07,
    day08,
    day09,
    day10,
    day11,
    day12,
    day13,
    day14,
    day15,
    day16,
    day17,
    day18,
    day19,
    day20,
    day21,
    day22,
    day23,
    day24,
    day25
};

type Solution = fn(&str) -> String;
static SOLUTIONS: [(Solution, Solution); 25] = 
    [(day01::part1::part1, day01::part2::part2),
     (day02::part1::part1, day02::part2::part2),
     (day03::part1::part1, day03::part2::part2),
     (day04::part1::part1, day04::part2::part2),
     (day05::part1::part1, day05::part2::part2),
     (day06::part1::part1, day06::part2::part2),
     (day07::part1::part1, day07::part2::part2),
     (day08::part1::part1, day08::part2::part2),
     (day09::part1::part1, day09::part2::part2),
     (day10::part1::part1, day10::part2::part2),
     (day11::part1::part1, day11::part2::part2),
     (day12::part1::part1, day12::part2::part2),
     (day13::part1::part1, day13::part2::part2),
     (day14::part1::part1, day14::part2::part2),
     (day15::part1::part1, day15::part2::part2),
     (day16::part1::part1, day16::part2::part2),
     (day17::part1::part1, day17::part2::part2),
     (day18::part1::part1, day18::part2::part2),
     (day19::part1::part1, day19::part2::part2),
     (day20::part1::part1, day20::part2::part2),
     (day21::part1::part1, day21::part2::part2),
     (day22::part1::part1, day22::part2::part2),
     (day23::part1::part1, day23::part2::part2),
     (day24::part1::part1, day24::part2::part2),
     (day25::part1::part1, empty)];

fn main() {
    let mut args = env::args().skip(1);
    let day = args.next().expect("Must have at least two arguments.").parse::<usize>().expect("Failed to parse arguments.");
    let part = args.next().expect("Must have at least two arguments.").parse::<usize>().expect("Failed to parse arguments.");
    let input;
    if let Some(filename) = args.next() {
        input = fs::read_to_string(filename).unwrap();
    } else {
        input = fs::read_to_string(format!("inputs/day{:02}.txt", day)).unwrap();
    }
    
    let ans;
    match part {
        1 => ans = SOLUTIONS[day - 1].0(input.as_str()),
        2 => ans = SOLUTIONS[day - 1].1(input.as_str()),
        _ => panic!("Part must be 1 or 2"),
    }
    println!("{}", ans);
}

fn empty(_: &str) -> String {
    String::from("")
}