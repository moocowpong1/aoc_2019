
use crate::day03::part1::{Instruction, parse_input, Point};
use std::mem;


// We need to rework the Segment structs to track the circuit distance from the origin,
// which requires rewriting a lot of part 1 code
// We also reverse our assumption about orientation from part 1; now segments can be pointing
// either direction because the two orientations of the segment are no longer equivalent.
#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct HorizSegment {
    initial: Point,
    extent: i64,
    signal_delay: i64,
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct VertSegment {
    initial: Point,
    extent: i64,
    signal_delay: i64,
}

// Returns horizontal and vertical segments as separate vecs
pub fn line_to_segments(line: Vec<Instruction>) -> (Vec<HorizSegment>, Vec<VertSegment>) {
    let mut loc = Point::origin();
    let mut horiz_segments = Vec::new();
    let mut vert_segments = Vec::new();
    let mut signal_delay = 0;

    for inst in line {
        let next_loc = loc.after_step(&inst);
        let horizontal = inst.is_horizontal();
        let extent = inst.extent();

        if horizontal {
            horiz_segments.push(HorizSegment { initial: loc, extent, signal_delay });
        } else {
            vert_segments.push(VertSegment { initial: loc, extent, signal_delay });
        }
        loc = next_loc;
        signal_delay += extent.abs();
    }

    (horiz_segments, vert_segments)
}

// Returns the intersection point if it exists, along with the total signal delay along both paths
pub fn intersection(horiz: &HorizSegment, vert: &VertSegment) -> Option<(Point, i64)> {
    let possible_intersection = Point { x: vert.initial.x, y: horiz.initial.y };

    let mut x_low = horiz.initial.x;
    let mut x_high = x_low + horiz.extent;
    if x_high < x_low { mem::swap(&mut x_low, &mut x_high) }
    let mut y_low = vert.initial.y;
    let mut y_high = y_low + vert.extent;
    if y_high < y_low { mem::swap(&mut y_low, &mut y_high) }

    if (possible_intersection.x < x_low) || (possible_intersection.x > x_high) { return None }
    if (possible_intersection.y < y_low) || (possible_intersection.y > y_high) { return None }

    let signal_delay_h = horiz.signal_delay + (possible_intersection.x - horiz.initial.x).abs();
    let signal_delay_v = vert.signal_delay + (possible_intersection.y - vert.initial.y).abs();

    Some((possible_intersection, signal_delay_h + signal_delay_v))
}



pub fn part2(input: &str) -> String {

    let (line1, line2) = parse_input(input);

    let (horiz1, vert1) = line_to_segments(line1);
    let (horiz2, vert2) = line_to_segments(line2);

    let mut min_signal_delay: i64 = i64::MAX;

    for h in horiz1.iter() {
        for v in vert2.iter() {
            if let Some((_, signal_delay)) = intersection(h, v) {
                if signal_delay == 0 {continue}
                min_signal_delay = i64::min(min_signal_delay, signal_delay);
            }
        }
    }

    for v in vert1.iter() {
        for h in horiz2.iter() {
            if let Some((_, signal_delay)) = intersection(h, v) {
                if signal_delay == 0 {continue}
                min_signal_delay = i64::min(min_signal_delay, signal_delay);
            }
        }
    }

    format!("{}", min_signal_delay)
}

#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day03_part2() {
        let input = fs::read_to_string("inputs/day03.txt").unwrap();
        let ans = super::part2(input.as_str());
        assert!(ans == "14358");
    }
}