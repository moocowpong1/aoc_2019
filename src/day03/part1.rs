

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub enum Instruction {
    Up(i64),
    Right(i64),
    Down(i64),
    Left(i64),
}
use Instruction::*;

impl Instruction {
    pub fn is_horizontal(&self) -> bool {
        match self {
            Up(_) | Down(_) => false,
            Left(_) | Right(_) => true,
        }
    }

    pub fn extent(&self) -> i64 {
        match self {
            Up(val) | Right(val) => *val,
            Down(val) | Left(val) => -*val,
        }
    }
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct Point {
    pub x: i64,
    pub y: i64,
}

impl Point {
    pub fn manhattan_norm(&self) -> i64 {
        self.x.abs() + self.y.abs()
    }

    pub fn after_step(&self, inst: &Instruction) -> Point {
        match inst.is_horizontal() {
            true => Point { x: self.x + inst.extent(), ..*self },
            false => Point { y: self.y + inst.extent(), ..*self },
        }
    }

    pub fn origin() -> Point { Point { x: 0, y: 0 } }
}

// In each case we declare that the segment will be "pointing" up or right, so extent will be positive.
#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct HorizSegment {
    initial: Point,
    extent: i64,
}

#[derive(PartialEq, Eq, Clone, Copy, Debug)]
pub struct VertSegment {
    initial: Point,
    extent: i64,
}

pub fn parse_input(input: &str) -> (Vec<Instruction>, Vec<Instruction>) {
    let mut input_lines = input.lines();
    let mut line1 = Vec::new();
    let mut line2 = Vec::new();

    for line in vec![&mut line1, &mut line2] {
        let parts = input_lines.next().unwrap().split(',');
        for part in parts {
            let code = part.as_bytes()[0];
            let val = part[1..].parse::<i64>().unwrap();
            let instr = match code {
                b'U' => Up(val),
                b'R' => Right(val),
                b'D' => Down(val),
                b'L' => Left(val),
                _ => panic!("Unrecognized instruction: {}", code as char),
            };
            line.push(instr);
        }
    }

    (line1, line2)
}

// Returns horizontal and vertical segments as separate vecs
pub fn line_to_segments(line: Vec<Instruction>) -> (Vec<HorizSegment>, Vec<VertSegment>) {
    let mut loc = Point::origin();
    let mut horiz_segments = Vec::new();
    let mut vert_segments = Vec::new();

    for inst in line {
        let next_loc = loc.after_step(&inst);
        let horizontal = inst.is_horizontal();
        let mut extent = inst.extent();
        let initial;
        if extent >= 0 {
            initial = loc;
        } else {        // if extent is negative, reverse the segment
            initial = next_loc;
            extent = -extent;
        }
        if horizontal {
            horiz_segments.push(HorizSegment { initial, extent });
        } else {
            vert_segments.push(VertSegment { initial, extent });
        }
        loc = next_loc;
    }

    (horiz_segments, vert_segments)
}

pub fn intersection(horiz: &HorizSegment, vert: &VertSegment) -> Option<Point> {
    let possible_intersection = Point { x: vert.initial.x, y: horiz.initial.y };

    let x_low = horiz.initial.x;
    let x_high = x_low + horiz.extent;
    let y_low = vert.initial.y;
    let y_high = y_low + vert.extent;

    if (possible_intersection.x < x_low) || (possible_intersection.x > x_high) { return None }
    if (possible_intersection.y < y_low) || (possible_intersection.y > y_high) { return None }

    Some(possible_intersection)
}

pub fn part1(input: &str) -> String {
    let (line1, line2) = parse_input(input);
    
    // Assumption: lines in our input alternate between horizontal and vertical steps. This means that we can limit
    // ourselves to checking crossings of horizontal with vertical segments.

    let (horiz1, vert1) = line_to_segments(line1);
    let (horiz2, vert2) = line_to_segments(line2);

    let mut min_distance: i64 = i64::MAX;

    for h in horiz1.iter() {
        for v in vert2.iter() {
            if let Some(point) = intersection(h, v) {
                let norm = point.manhattan_norm();
                if norm == 0 { continue; }
                min_distance = i64::min(min_distance, norm);
            }
        }
    }

    for v in vert1.iter() {
        for h in horiz2.iter() {
            if let Some(point) = intersection(h, v) {
                let norm = point.manhattan_norm();
                if norm == 0 { continue; }
                min_distance = i64::min(min_distance, norm);
            }
        }
    }

    format!("{}", min_distance)
}






#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day03_part1() {
        let input = fs::read_to_string("inputs/day03.txt").unwrap();
        let ans = super::part1(input.as_str());
        assert!(ans == "1519");
    }
}