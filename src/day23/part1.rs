

pub fn part1(input: &str) -> String {
    let mut nums = Vec::new();
    for line in input.lines() {
        nums.push(line.parse::<u64>().unwrap());
    }
    nums.iter().sum::<u64>().to_string()
}

#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day23_part1() {
        let input = fs::read_to_string("inputs/day23.txt").unwrap();
        let _ans = super::part1(input.as_str());
        assert!(true);
    }
}