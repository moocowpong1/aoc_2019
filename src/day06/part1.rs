use std::collections::HashMap;

pub struct Body<'a> {
    pub parent: Option<&'a str>,
    pub children: Vec<&'a str>,
}

pub fn parse_input(input: &str) -> HashMap<&str, Body> {
    let mut bodies: HashMap<&str, Body> = HashMap::new();
    for line in input.lines() {
        let mut names = line.split(')');
        let parent = names.next().unwrap();
        let child = names.next().unwrap();
        bodies.entry(child)
              .and_modify(|cb| { assert!(cb.parent == None); cb.parent = Some(parent); })
              .or_insert(Body { parent: Some(parent), children: Vec::new()});
        bodies.entry(parent)
              .and_modify(|pb| { assert!(!pb.children.contains(&child)); pb.children.push(child); })
              .or_insert(Body { parent: None, children: vec![child]});
    }
    bodies
}

pub fn compute_descendants<'a>(bodies: &HashMap<&'a str, Body<'a>>) -> HashMap<&'a str, u64> {
    let mut descendant_counts = HashMap::new();
    
    compute_descendants_from(&"COM", bodies, &mut descendant_counts);

    descendant_counts
}

fn compute_descendants_from<'a>(current: &'a str, bodies: &HashMap<&'a str, Body<'a>>, descendant_counts: &mut HashMap<&'a str, u64>) -> u64 {
    let body = bodies.get(current).unwrap();
    let mut count = 0;
    for child in body.children.iter() {
        count += 1 + compute_descendants_from(*child, bodies, descendant_counts);
    }
    descendant_counts.insert(current, count);
    count
}

pub fn part1(input: &str) -> String {

    let bodies = parse_input(input);
    let counts = compute_descendants(&bodies);

    format!("{}", counts.values().sum::<u64>())
}






#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day06_part1() {
        let input = fs::read_to_string("inputs/day06.txt").unwrap();
        let ans = super::part1(input.as_str());
        assert!(ans == "249308");
    }
}