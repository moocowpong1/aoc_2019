
use crate::day06::part1;
use std::collections::HashMap;

fn object_branch<'a>(mut current: &'a str, bodies: &HashMap<&'a str, part1::Body<'a>>) -> Vec<&'a str> {
    let mut branch = Vec::new();
    loop {
        if let Some(parent) = bodies.get(current).unwrap().parent {
            branch.push(parent);
            current = parent;
        } else {
            break;
        }
    }
    branch
}

pub fn part2(input: &str) -> String {
    let bodies = part1::parse_input(input);

    let you_path = object_branch(&"YOU", &bodies);
    let santa_path = object_branch(&"SAN", &bodies);

    let mut you_cursor = you_path.len() - 1;
    let mut santa_cursor = santa_path.len() - 1;

    loop {
        if you_path[you_cursor] == santa_path[santa_cursor] {
            you_cursor -= 1;
            santa_cursor -= 1;
        } else {
            break;
        }
    }

    format!("{}", santa_cursor + you_cursor + 2)
}








#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day06_part2() {
        let input = fs::read_to_string("inputs/day06.txt").unwrap();
        let ans = super::part2(input.as_str());
        assert!(ans == "349");
    }
}