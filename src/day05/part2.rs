
use crate::intcode::IntcodeMachine;

pub fn part2(input: &str) -> String {
    let mut machine = IntcodeMachine::initialize(input, 5).unwrap();
    machine.write_input(vec![5]);
    machine.execute().unwrap();
    format!("{}", machine.outputs().last().unwrap())
}








#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day05_part2() {
        let input = fs::read_to_string("inputs/day05.txt").unwrap();
        let ans = super::part2(input.as_str());
        assert!(ans == "742621");
    }
}