use crate::intcode::IntcodeMachine;

pub fn part1(input: &str) -> String {

    let mut machine = IntcodeMachine::initialize(input, 5).unwrap();
    machine.write_input(vec![1]);
    machine.execute().unwrap(); // panic on error
    format!("{}", machine.outputs().last().unwrap())
}

#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day05_part1() {
        let input = fs::read_to_string("inputs/day05.txt").unwrap();
        let ans = super::part1(input.as_str());
        assert!(ans == "9961446");
    }
}