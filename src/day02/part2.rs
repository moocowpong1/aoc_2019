
use crate::intcode::IntcodeMachine;

const TARGET_OUTPUT:i64 = 19690720;

pub fn part2(input: &str) -> String {

    let intcode_machine = IntcodeMachine::initialize(input, 2).unwrap();
    
    for noun in 0..100 {
        for verb in 0..100 {
            let mut machine = intcode_machine.clone();
            let memory = machine.memory_mut();
            memory[1] = noun;
            memory[2] = verb;
            if let Err(_) = machine.execute() { break; } // skip this pair if it leads to an invalid action
            let output = machine.memory()[0];
            if output == TARGET_OUTPUT {
                return format!("{:02}{:02}", noun, verb);
            }
        }
    }

    panic!("No noun + verb combination found.")
}




#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day02_part2() {
        let input = fs::read_to_string("inputs/day02.txt").unwrap();
        let ans = super::part2(input.as_str());
        assert!(ans == "9425");
    }
}