
use crate::intcode::IntcodeMachine;

pub fn part1(input: &str) -> String {
    
    let mut intcode_machine = IntcodeMachine::initialize(input, 2).unwrap();
    let memory = intcode_machine.memory_mut();

    memory[1] = 12;
    memory[2] = 2;

    intcode_machine.execute().unwrap(); // panic if there's an error

    format!("{}", intcode_machine.memory()[0])
}


#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day02_part1() {
        let input = fs::read_to_string("inputs/day02.txt").unwrap();
        let ans = super::part1(input.as_str());
        assert!(ans == "3085697");
    }
}