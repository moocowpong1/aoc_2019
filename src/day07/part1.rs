
use crate::intcode::IntcodeMachine;

pub fn part1(input: &str) -> String {
    let amplifier = IntcodeMachine::initialize(input, 7u8).unwrap();
    let mut max_thruster_signal:i64 = i64::MIN;

    for phase0 in 0..=4 {
        for phase1 in 0..=4 {
            if phase1 == phase0 { continue; }
            for phase2 in 0..=4 {
                if phase2 == phase0 || phase2 == phase1 { continue; }
                for phase3 in 0..=4 {
                    if phase3 == phase0 || phase3 == phase1 || phase3 == phase2 { continue; }
                    for phase4 in 0..=4 {
                        if phase4 == phase0 || phase4 == phase1 || phase4 == phase2 || phase4 == phase3 { continue; }
                        let mut amplifiers = vec![amplifier.clone(); 5];
                        let phases = [phase0, phase1, phase2, phase3, phase4];
                        let mut signal = 0i64;
                        for i in 0..5 {
                            amplifiers[i].write_input(vec![phases[i], signal]);
                            amplifiers[i].execute().unwrap();
                            signal = amplifiers[i].outputs().next().unwrap();
                        }
                        if signal > max_thruster_signal {
                            max_thruster_signal = signal;
                        }
                    }
                }
            }
        }
    }


    format!("{}", max_thruster_signal)
}

#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day07_part1() {
        let input = fs::read_to_string("inputs/day07.txt").unwrap();
        let ans = super::part1(input.as_str());
        assert!(ans == "45730");
    }
}