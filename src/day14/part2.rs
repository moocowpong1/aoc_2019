
//use crate::day14::part1;

pub fn part2(_input: &str) -> String {
    String::from("0")
}








#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day14_part2() {
        let input = fs::read_to_string("inputs/day14.txt").unwrap();
        let _ans = super::part2(input.as_str());
        assert!(true);
    }
}