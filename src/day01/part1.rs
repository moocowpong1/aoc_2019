

pub fn part1(input: &str) -> String {
    let mut nums = Vec::new();
    for line in input.lines() {
        nums.push(line.parse::<i64>().unwrap());
    }
    nums.iter().copied().map(calc_fuel).sum::<i64>().to_string()
}

pub fn calc_fuel(mass: i64) -> i64 {
    mass/3 - 2
}





#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day01_part1() {
        let input = fs::read_to_string("inputs/day01.txt").unwrap();
        let ans = super::part1(input.as_str());
        assert!(ans == "3329926");
    }
}