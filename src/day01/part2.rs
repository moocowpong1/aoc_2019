
use crate::day01::part1;

pub fn part2(input: &str) -> String {
    let mut masses: Vec<i64> = Vec::new();
    for line in input.lines() {
        masses.push(line.parse::<i64>().unwrap());
    }
    let fuel: i64 = masses.iter().copied().map(calc_fuel_recursive).sum();
    format!("{}", fuel)
}

fn calc_fuel_recursive(mut mass: i64) -> i64 {
    let mut total_fuel = 0;
    loop {
        let fuel = part1::calc_fuel(mass);
        if fuel <= 0 { break; }
        total_fuel += fuel;
        mass = fuel;
    }
    total_fuel
}






#[cfg(test)]
mod tests {
    use std::fs;

    #[test]
    fn test_day01_part2() {
        let input = fs::read_to_string("inputs/day01.txt").unwrap();
        let ans = super::part2(input.as_str());
        assert!(ans == "4992008");
    }
}