
use std::fs;
use criterion::{criterion_group, criterion_main, Criterion};
use aoc_2019;

criterion_main!(day01, day02, day03, day04, day05, day06, day07, day08, day09, day10,
                day11, day12, day13, day14, day15, day16, day17, day18, day19, day20,
                day21, day22, day23, day24, day25);



fn day01part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day01.txt").unwrap();
    c.bench_function("day01part1", |b| b.iter(|| aoc_2019::day01::part1::part1(input.as_str())));
}

fn day01part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day01.txt").unwrap();
    c.bench_function("day01part2", |b| b.iter(|| aoc_2019::day01::part2::part2(input.as_str())));
}

criterion_group!(day01, day01part1, day01part2);




fn day02part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day02.txt").unwrap();
    c.bench_function("day02part1", |b| b.iter(|| aoc_2019::day02::part1::part1(input.as_str())));
}

fn day02part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day02.txt").unwrap();
    c.bench_function("day02part2", |b| b.iter(|| aoc_2019::day02::part2::part2(input.as_str())));
}

criterion_group!(day02, day02part1, day02part2);




fn day03part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day03.txt").unwrap();
    c.bench_function("day03part1", |b| b.iter(|| aoc_2019::day03::part1::part1(input.as_str())));
}

fn day03part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day03.txt").unwrap();
    c.bench_function("day03part2", |b| b.iter(|| aoc_2019::day03::part2::part2(input.as_str())));
}

criterion_group!(day03, day03part1, day03part2);




fn day04part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day04.txt").unwrap();
    c.bench_function("day04part1", |b| b.iter(|| aoc_2019::day04::part1::part1(input.as_str())));
}

fn day04part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day04.txt").unwrap();
    c.bench_function("day04part2", |b| b.iter(|| aoc_2019::day04::part2::part2(input.as_str())));
}

criterion_group!(day04, day04part1, day04part2);




fn day05part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day05.txt").unwrap();
    c.bench_function("day05part1", |b| b.iter(|| aoc_2019::day05::part1::part1(input.as_str())));
}

fn day05part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day05.txt").unwrap();
    c.bench_function("day05part2", |b| b.iter(|| aoc_2019::day05::part2::part2(input.as_str())));
}

criterion_group!(day05, day05part1, day05part2);




fn day06part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day06.txt").unwrap();
    c.bench_function("day06part1", |b| b.iter(|| aoc_2019::day06::part1::part1(input.as_str())));
}

fn day06part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day06.txt").unwrap();
    c.bench_function("day06part2", |b| b.iter(|| aoc_2019::day06::part2::part2(input.as_str())));
}

criterion_group!(day06, day06part1, day06part2);




fn day07part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day07.txt").unwrap();
    c.bench_function("day07part1", |b| b.iter(|| aoc_2019::day07::part1::part1(input.as_str())));
}

fn day07part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day07.txt").unwrap();
    c.bench_function("day07part2", |b| b.iter(|| aoc_2019::day07::part2::part2(input.as_str())));
}

criterion_group!(day07, day07part1, day07part2);




fn day08part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day08.txt").unwrap();
    c.bench_function("day08part1", |b| b.iter(|| aoc_2019::day08::part1::part1(input.as_str())));
}

fn day08part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day08.txt").unwrap();
    c.bench_function("day08part2", |b| b.iter(|| aoc_2019::day08::part2::part2(input.as_str())));
}

criterion_group!(day08, day08part1, day08part2);




fn day09part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day09.txt").unwrap();
    c.bench_function("day09part1", |b| b.iter(|| aoc_2019::day09::part1::part1(input.as_str())));
}

fn day09part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day09.txt").unwrap();
    c.bench_function("day09part2", |b| b.iter(|| aoc_2019::day09::part2::part2(input.as_str())));
}

criterion_group!(day09, day09part1, day09part2);




fn day10part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day10.txt").unwrap();
    c.bench_function("day10part1", |b| b.iter(|| aoc_2019::day10::part1::part1(input.as_str())));
}

fn day10part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day10.txt").unwrap();
    c.bench_function("day10part2", |b| b.iter(|| aoc_2019::day10::part2::part2(input.as_str())));
}

criterion_group!(day10, day10part1, day10part2);




fn day11part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day11.txt").unwrap();
    c.bench_function("day11part1", |b| b.iter(|| aoc_2019::day11::part1::part1(input.as_str())));
}

fn day11part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day11.txt").unwrap();
    c.bench_function("day11part2", |b| b.iter(|| aoc_2019::day11::part2::part2(input.as_str())));
}

criterion_group!(day11, day11part1, day11part2);




fn day12part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day12.txt").unwrap();
    c.bench_function("day12part1", |b| b.iter(|| aoc_2019::day12::part1::part1(input.as_str())));
}

fn day12part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day12.txt").unwrap();
    c.bench_function("day12part2", |b| b.iter(|| aoc_2019::day12::part2::part2(input.as_str())));
}

criterion_group!(day12, day12part1, day12part2);




fn day13part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day13.txt").unwrap();
    c.bench_function("day13part1", |b| b.iter(|| aoc_2019::day13::part1::part1(input.as_str())));
}

fn day13part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day13.txt").unwrap();
    c.bench_function("day13part2", |b| b.iter(|| aoc_2019::day13::part2::part2(input.as_str())));
}

criterion_group!(day13, day13part1, day13part2);




fn day14part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day14.txt").unwrap();
    c.bench_function("day14part1", |b| b.iter(|| aoc_2019::day14::part1::part1(input.as_str())));
}

fn day14part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day14.txt").unwrap();
    c.bench_function("day14part2", |b| b.iter(|| aoc_2019::day14::part2::part2(input.as_str())));
}

criterion_group!(day14, day14part1, day14part2);




fn day15part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day15.txt").unwrap();
    c.bench_function("day15part1", |b| b.iter(|| aoc_2019::day15::part1::part1(input.as_str())));
}

fn day15part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day15.txt").unwrap();
    c.bench_function("day15part2", |b| b.iter(|| aoc_2019::day15::part2::part2(input.as_str())));
}

criterion_group!(day15, day15part1, day15part2);




fn day16part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day16.txt").unwrap();
    c.bench_function("day16part1", |b| b.iter(|| aoc_2019::day16::part1::part1(input.as_str())));
}

fn day16part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day16.txt").unwrap();
    c.bench_function("day16part2", |b| b.iter(|| aoc_2019::day16::part2::part2(input.as_str())));
}

criterion_group!(day16, day16part1, day16part2);




fn day17part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day17.txt").unwrap();
    c.bench_function("day17part1", |b| b.iter(|| aoc_2019::day17::part1::part1(input.as_str())));
}

fn day17part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day17.txt").unwrap();
    c.bench_function("day17part2", |b| b.iter(|| aoc_2019::day17::part2::part2(input.as_str())));
}

criterion_group!(day17, day17part1, day17part2);




fn day18part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day18.txt").unwrap();
    c.bench_function("day18part1", |b| b.iter(|| aoc_2019::day18::part1::part1(input.as_str())));
}

fn day18part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day18.txt").unwrap();
    c.bench_function("day18part2", |b| b.iter(|| aoc_2019::day18::part2::part2(input.as_str())));
}

criterion_group!(day18, day18part1, day18part2);




fn day19part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day19.txt").unwrap();
    c.bench_function("day19part1", |b| b.iter(|| aoc_2019::day19::part1::part1(input.as_str())));
}

fn day19part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day19.txt").unwrap();
    c.bench_function("day19part2", |b| b.iter(|| aoc_2019::day19::part2::part2(input.as_str())));
}

criterion_group!(day19, day19part1, day19part2);




fn day20part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day20.txt").unwrap();
    c.bench_function("day20part1", |b| b.iter(|| aoc_2019::day20::part1::part1(input.as_str())));
}

fn day20part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day20.txt").unwrap();
    c.bench_function("day20part2", |b| b.iter(|| aoc_2019::day20::part2::part2(input.as_str())));
}

criterion_group!(day20, day20part1, day20part2);




fn day21part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day21.txt").unwrap();
    c.bench_function("day21part1", |b| b.iter(|| aoc_2019::day21::part1::part1(input.as_str())));
}

fn day21part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day21.txt").unwrap();
    c.bench_function("day21part2", |b| b.iter(|| aoc_2019::day21::part2::part2(input.as_str())));
}

criterion_group!(day21, day21part1, day21part2);




fn day22part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day22.txt").unwrap();
    c.bench_function("day22part1", |b| b.iter(|| aoc_2019::day22::part1::part1(input.as_str())));
}

fn day22part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day22.txt").unwrap();
    c.bench_function("day22part2", |b| b.iter(|| aoc_2019::day22::part2::part2(input.as_str())));
}

criterion_group!(day22, day22part1, day22part2);




fn day23part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day23.txt").unwrap();
    c.bench_function("day23part1", |b| b.iter(|| aoc_2019::day23::part1::part1(input.as_str())));
}

fn day23part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day23.txt").unwrap();
    c.bench_function("day23part2", |b| b.iter(|| aoc_2019::day23::part2::part2(input.as_str())));
}

criterion_group!(day23, day23part1, day23part2);




fn day24part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day24.txt").unwrap();
    c.bench_function("day24part1", |b| b.iter(|| aoc_2019::day24::part1::part1(input.as_str())));
}

fn day24part2(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day24.txt").unwrap();
    c.bench_function("day24part2", |b| b.iter(|| aoc_2019::day24::part2::part2(input.as_str())));
}

criterion_group!(day24, day24part1, day24part2);




fn day25part1(c: &mut Criterion) {
    let input = fs::read_to_string("inputs/day25.txt").unwrap();
    c.bench_function("day25part1", |b| b.iter(|| aoc_2019::day25::part1::part1(input.as_str())));
}

criterion_group!(day25, day25part1);




